import { supportsWebauthn, startAttestation, startAssertion } from '@simplewebauthn/browser';
import './App.css';

function App() {


  const onReg = async () => {
    const resp = await fetch('http://localhost:5000/api/v0/webauth/generate-attestation-options', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: 'alex3@example.com'
      }),
    });

    const attResp = await startAttestation(await resp.json());
    const verificationResp = await fetch('http://localhost:5000/api/v0/webauth/verify-attestation', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(attResp),
    });

    const verificationJSON = await verificationResp.json();
    console.log(verificationJSON);
  }

  const onLogin = async () => {
    const resp = await fetch('http://localhost:5000/api/v0/webauth/generate-assertion-options', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: 'alex3@example.com'
      }),
    });

    const attResp = await startAssertion(await resp.json());
    const verificationResp = await fetch('http://localhost:5000/api/v0/webauth/verify-assertion', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(attResp),
    });

    const verificationJSON = await verificationResp.json();
    console.log(verificationJSON);
  }

  return (
    <div className="App">
      <h1>Start</h1><br/>
      {
        supportsWebauthn() && <>
          <button onClick={onReg}>register</button>
          <button onClick={onLogin}>login</button>
        </>
      }
    </div>
  );
}

export default App;
